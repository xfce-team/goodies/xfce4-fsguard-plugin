# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Rihards Priedītis <rprieditis@gmail.com>, 2009
# Rihards Prieditis <rprieditis@inbox.lv>, 2007
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-07-03 21:00+0200\n"
"PO-Revision-Date: 2017-09-19 18:06+0000\n"
"Last-Translator: Nick Schermer <nick@xfce.org>\n"
"Language-Team: Latvian (http://www.transifex.com/xfce/xfce-panel-plugins/language/lv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: lv\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);\n"

#: ../panel-plugin/fsguard.c:252
msgid "Unable to find an appropriate application to open the mount point"
msgstr "Nevarēja atrast piemērotu lietotni, lai atvērtu montējuma punktu"

#: ../panel-plugin/fsguard.c:288 ../panel-plugin/fsguard.c:289
#, c-format
msgid "%.2f GB"
msgstr "%.2f GB"

#: ../panel-plugin/fsguard.c:292
#, c-format
msgid "%s/%s space left on %s (%s)"
msgstr "%s/%s brīvās vietas palicis uz %s (%s)"

#: ../panel-plugin/fsguard.c:292
#, c-format
msgid "%s/%s space left on %s"
msgstr "%s/%s brīvās vietas palicis uz %s"

#: ../panel-plugin/fsguard.c:295 ../panel-plugin/fsguard.c:296
#, c-format
msgid "%.0f MB"
msgstr "%.0f MB"

#: ../panel-plugin/fsguard.c:298
#, c-format
msgid "could not check mountpoint %s, please check your config"
msgstr "nevarēja pārbaudīt montēšanas punktu %s, lūdzu pārbaudiet uzstādījumus"

#: ../panel-plugin/fsguard.c:318
#, c-format
msgid "Only %s space left on %s (%s)!"
msgstr "Tikai %s brīvās vietas palicis uz %s (%s)!"

#: ../panel-plugin/fsguard.c:321
#, c-format
msgid "Only %s space left on %s!"
msgstr "Tikai %s brīvas vietas plicis uz %s!"

#. }}}
#. vim600: set foldmethod=marker: foldmarker={{{,}}}
#: ../panel-plugin/fsguard.c:635 ../panel-plugin/fsguard.desktop.in.h:1
msgid "Free Space Checker"
msgstr "Brīvās Vietas Pārbaudītājs"

#: ../panel-plugin/fsguard.c:646
msgid "Configuration"
msgstr "Konfigurācija"

#: ../panel-plugin/fsguard.c:653
msgid "Mount point"
msgstr "Montēšanas punkts"

#: ../panel-plugin/fsguard.c:659
msgid "Warning limit (%)"
msgstr "Brīdinājuma limits (%)"

#: ../panel-plugin/fsguard.c:664
msgid "Urgent limit (%)"
msgstr "Kritisks limits (%)"

#: ../panel-plugin/fsguard.c:684
msgid "User Interface"
msgstr "Lietotāja saskarne"

#: ../panel-plugin/fsguard.c:691
msgid "Name"
msgstr "Nosaukums"

#: ../panel-plugin/fsguard.c:699
msgid "Display size"
msgstr "Displeja izmērs"

#: ../panel-plugin/fsguard.c:703
msgid "Display meter"
msgstr "Rādīt skaitītāju"

#: ../panel-plugin/fsguard.c:707
msgid "Display button"
msgstr "Rādīt skaitītāju"

#: ../panel-plugin/fsguard.desktop.in.h:2
msgid "Monitor free disk space"
msgstr "Uzrauga brīvo diska vietu"
